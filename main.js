var express = require('express'),
	app 	= express(),
	http 	= require('http').Server(app),
	io 		= require('socket.io')(http),
	Twitter = require('twit'),
	config 	= require('./config.json'),
	twitter = new Twitter(config),
	session = require('express-session'),
	cookieParser = require('cookie-parser');
	bodyParser = require('body-parser');
	mongoose = require('mongoose'),
	crypto = require('crypto'),
	db = mongoose.connection,
	Schema = mongoose.Schema;
	var SongSchema,  SongModel, UserSchema, UserModel;
	var	port 	= 3000;

// Connecting to database
db.on('error', function(err){
	console.log("Error in database connection");
	console.log(err);
});

db.once('open', function (){
	console.log("Conneced to database.");
	setupEntities();
	retrievingRecords();
	checkCreateDefaultUser();
});

// Establishing connection with the mongodb on the local machine
mongoose.connect('mongodb://127.0.0.1/wtwilt');

var currentDate = new Date();
exitflag=false;
var text = [],
	latitude =[],
	longitude =[];

app.use(express.static(__dirname + '/public'));
//Express Configuration
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({secret:"sadf3234",saveUninitialized: true,resave: true }));

// Filtering based on specified hashtags
var stream = twitter.stream('statuses/filter', { track: ['#np', '#NP', '#nowplaying', '#NOWPLAYING', 'NowPlaying'], language: 'en' })

stream.on('tweet', function (tweet) {
	var song = [];
	var songname='';
	var artistename='';
	var artiste = [];
	var encountered = false;

	if (tweet.geo != null){
        var temp = "";
		temp = tweet.text.toLowerCase().replace("download it now at", "");
        temp = temp.replace('"','');
		text = temp.split(" ");
        latitude = tweet.geo.coordinates[0];
		longitude = tweet.geo.coordinates[1];
		console.log(tweet.text);
		for(i = 0; i < text.length; i++){
			if(text[i] == 'by' || text[i]=='from' || text[i]=='-') encountered = true;
			if(encountered == false){
				if(text[i].indexOf('#')=== -1 && text[i] != 'by' && text[i] != 'from' && text[i].indexOf('http') === -1 &&  text[i] != '-')
					song = song.concat(text[i]);
			}
			else{
				if(text[i].indexOf('#') === -1 && text[i] != 'by' && text[i] != 'from' && text[i].indexOf('http') === -1 && text[i] != '-' )
					artiste = artiste.concat(text[i]);
			}
        }
		for (var j = 0; j < song.length; j++) songname+= song[j]+' ';
		for (var k = 0; k < artiste.length; k++) artistename+= artiste[k]+' ';
		songname = songname.toLowerCase().trim();
		artistename = artistename.toLowerCase().trim();

		var indexArtiste = artistename.indexOf("is in");
		if(indexArtiste>1) artistename = artistename.slice(0,indexArtiste);

		console.log("Stream: song: "+ songname +" artiste: "+ artistename);
		
		addingTweetInfo(songname, artistename, longitude, latitude);
    }
});
  
io.on('connection', function(socket){
	console.log('User connected ... Starting Stream connection');

	stream.on('tweet', function(tweet){
		io.emit('new tweet' ,tweet);
	});

	socket.on('disconnect', function(){
		console.log("User disconnected");
		stream.stop();
	});
});

function setupEntities(){
	// Defining how the data is stored in the database
	UserSchema = Schema({
		username	: String,
		email 		: String,
		password	: String,
		salt		: String
	});

	SongSchema = Schema({
 		song		: String,
		artiste		: String,
 		longitude	: String,
 		latitude	: String,
 		count		: Number
  	});
 	
 	//Running before saving the record
	UserSchema.pre('save', function (next) {
		this.password = crypto
	  					.createHash('sha1')
	  					.update(this.password + this.salt)
	  					.digest('hex');
	  	next();
	});

	UserSchema.methods.authenticate = function(password, callback){
		
		var tempHash = crypto
					.createHash('sha1')
					.update(password + this.salt)
					.digest('hex');
		
		if (tempHash === this.password){
			if (callback){
				callback(this);
			}
			return true;
		}
		callback(null);
		return false;
	}

 	//Registering the data definition as a user and a song within the database
 	UserModel = mongoose.model('User',UserSchema);
 	SongModel = mongoose.model('Song',SongSchema);
}

function checkCreateDefaultUser(){
	UserModel.find({}, function(err, data){
		if (err)return;
		if (data.length < 1){
			console.log("No User Detected: Creating Default User");
			var u = new UserModel({
				username : 'admin',
				password : 'password', 
			});

			u.save(function(err, users){
				if (err)console.log("Error occured while saving user "+ err);
				else console.log("User saved Successfully");
				console.log(users); 
			});
		}
	});
}

function addingTweetInfo(songname, artistename, longval, latval){
	//SongModel = mongoose.model('Song',SongSchema);	
    SongModel = mongoose.model('Song',SongSchema);

    if (typeof songname != "undefined" && typeof artistename != "undefined"){
		console.log("Adding Song Information...");
		SongModel.find({"song":songname, "artiste":artistename}, function(err, songs){
			if (err){
				console.log("Error occurred: "+ err);
				res.status(500).send("Error Occurred");
			}
			else {
				if (songs.length > 0){
					console.log("Song and Artiste match found");
					var t = songs[0];
					var n = t.count
					n++;
					t.count = n;
					
					t.save(function(err,record){
						if (err){
							console.log("Error occurred "+ err);
						}
						else {
							console.log("Song information added successfully");
						}
					});
				}
				else {				
					var t = new SongModel({
						song		: songname, 
						artiste		: artistename,
				 		longitude	: longval,
				 		latitude	: latval,
				 		count		: 1
				  	});

					t.save(function(err,record){
						if (err){
							console.log("Song information not added "+ err);
						}
						else {
							console.log("Song information added successfully");
						}
					});
				}
			}
		});
	}
}

// allow user to register and create an account
app.post("/user/register", function(req,res){
	var uname = req.body.username;
	var emailaddress = req.body.email;
	var pwd = req.body.password;

	UserModel = mongoose.model('User', UserSchema);

	var u1 = new UserModel({
		username 	: uname,
		email 		: emailaddress,
		password 	: pwd,
		salt 		: Math.random().toString(36).substr(2,5)
	});

	u1.save(function(err, record){
		if (err)
            res.send("Register unsuccessful "+ err);
		else 
            res.send("Register successful");
	});
});

// allow user to login
app.post("/user/login", function(req,res){
	var emailaddress = req.body.email;
	var pwd = req.body.password;

	if (emailaddress != null && pwd != null){
		console.log("Email address and password supplied");

		UserModel.find({"email":emailaddress}, function(err,users){
			if (err){
				console.log("Error occurred: "+err);
				res.status(500).send("Unable to log in user");
			}
			else{
				if (users.length > 0){
					console.log("Successfully retrieved user from database");
					var u = users[0];
					u.authenticate(pwd, function(result){
						if (result){
							req.session.user = result;
							req.session.save(function(err){
								res.send("Login successful");
							});
						}
						else {
							res.send("Incorrect Email & Password");
						}
					})
				}
				else {
					console.log("Email address not found");
					res.send("Incorrect Username & Password");	
				}
			}
		});
	}
	else {
		res.send("Must supply an email and password");
	}
});

// allow user to change password
app.put("/user/password", function(req,res){
	var email = req.body.email;
	var oldpassword = req.body.oldpassword;
	var newpassword = req.body.newpassword
	UserModel.find({"email":email}, function(err, users){
		if (err){
			console.log("Error occured: "+ err);
			res.status(500).send("Unable to find the email");
		}
		else {
			if (users.length > 0){
				console.log("Found user with supplied email");
				var u = users[0];
				u.authenticate(oldpassword, function(result){
					if (result){
						u.password = newpassword;

						u.save(function(err, record){
							if (err){
								console.log("Password not updated: "+ err);
				            	res.send("Password not updated "+ err);
							}
							else{
								console.log("Password updated successfully");
								res.send("Password update successful");
							} 
			        	});
			        }
			        else {
			        	console.log("Incorrect password.");
			        	res.send("Incorrect password");
			        }
				})
			}
			else {
				console.log("No User found");
				res.status(500).send("Incorrect email and password.")
			}
		}
	});
});

// allows user to delete their account
app.delete("/user", function(req,res){
	var email = req.body.email;
	var password = req.body.password;

	UserModel.find({"email":email}, function(err, users){
		if (err){
			console.log("Error occured: "+ err);
			res.status(500).send("Unable to find the email");
		}
		else {
			if (users.length > 0){
				console.log("Successfully retrieved user from database.");
				var u = users[0];
				u.authenticate(password, function(result){
					if (result){
						console.log("Password correct. Account being deleted.");
			            UserModel.find({"email":email}).remove().exec();
                        res.send("Account deleted");
					}
					else{
						console.log("Password incorrect.");
						res.status(500).send("Password incorrect");
					} 
		        });
			}
		}
	});	
});


app.get("/tweets", function(req, res){
	var limit = parseInt(req.param("limit"));
    var offset = parseInt(req.param("offset"));
	SongModel.find({}, null, {sort: {count: 1}}, function(err, songs){
        if (err){
            console.log("Error occurred: " + err);
            res.status(500).send("Unable to find song");
        }else{
            if (songs.length > 0){
                if(songs.length > limit)
                    res.send(songs.slice(offset, offset + limit));
                else
                    res.send(songs);
            }else{ 
                console.log("No songs found");
                res.send("No songs with that name");
            }
        }
    }).skip(offset).limit(limit);
});


app.get("/songs", function(req, res){
    var query = req.param("query");
	SongModel.find({"song": query}, function(err, songs){
        if (err){
            console.log("error occurred: " + err);
            res.status(500).send("unable to find song");
        }
        else {
            if (songs.length > 0){
                
                res.send(songs);
            }
            else { 
                console.log("No songs found");
                res.send("No songs with that name");
            }
        }
    });
});


function retrievingRecords(){
	//retrieves all of the users in the database
	UserModel.find({}, function(err, users){
		console.log("Found %d Users", users.length);
		console.log(users);
	});
	
	//retrieves all the songs in the database
	SongModel.find({}, function(err, songs){
		console.log("Found %d Songs", songs.length);
		console.log(songs);
	});
}

http.listen(process.env.PORT || port, function(){
	console.log("Listening on http://127.0.0.1:"+port);
});