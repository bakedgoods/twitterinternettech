(function(window){

	var tweets = [],
		pointArray,
		limit = 20,
		loaded = 0;


	$(document).ready(function(){
		var socket = io.connect();
        //receives new tweets live and if they have geo data adds to map
		socket.on('new tweet', function(tweet){
			if(tweet.geo != null){
				pointArray.push(new google.maps.LatLng(tweet.geo.coordinates[0], tweet.geo.coordinates[1]));
                $("#live-tweet").text(tweet.text);
			}
		});
		google.maps.event.addDomListener(window, 'load', initialize);
        
        //sets on click listeners for elements
		$('#toggleHeatmap').click(toggleHeatmap);
		$('#changeGradient').click(changeGradient);
		$('#changeRadius').click(changeRadius);
		$('#changeOpacity').click(changeOpacity);
        $("#signup-btn").click(getRegisterInfo);
	    $("#signin-btn").click(getLoginInfo);
        $("#change-btn").click(changePassword);
        $("#signin-txt").click(switchButtons);
        $("#delete-txt").click(deleteUser);
        $("#appname").click(function(){
            loaded = 0;
            populateTable();
        });
    
        $("#more-btn").click(populateTable);
        
        populateTable();
        
        //gets username from cookie and shows in nav bar
        var user = JSON.parse(getCookie("user"));
        $(".nav_name").text(user.email);
	});
    
    //gets cookie by name
    var getCookie = function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
        }
        return "";
    }
    
    //switches the signin and signup buttons
    var switchButtons = function(){
        $("#signin-btn").show();
        $("#signup-btn").hide();
        $("#signin-txt").hide();
        $("#username_input").hide();
    };
    
    //initializes the map and heatmap layer
	var initialize = function() {
      var mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(30, 0),
        mapTypeId: google.maps.MapTypeId.SATELLITE
      };

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      pointArray = new google.maps.MVCArray(tweets);

      heatmap = new google.maps.visualization.HeatmapLayer({
        data: pointArray
      });

      heatmap.setMap(map);
    }
    
    //gets songs from the database and shows a table of its name and plays
    var populateTable = function(){
        console.log(limit + " " + loaded);
    	$.get("/tweets?limit=" + limit + "&offset=" + loaded, function(data){
      		loaded += data.length;
			data.forEach(function(el){
              //  if(el.count > 1)
				    $('#tbl_body').append("<tr><td>" + el.song + " - " + el.artiste + "</td><td>" + el.count + "</td></tr>");
			});
            if(loaded % 20 != 0)
                $('more-btn').prop('disabled', true);
		});
    }
    
    //toggles the heatmap
    var toggleHeatmap = function() {
 	  console.log("Heat Map toggled.");
      heatmap.setMap(heatmap.getMap() ? null : map);
    }
    
    //changes the gradient
    var changeGradient = function() {
    	console.log("Heat Map Gradient changed");
	    var gradient = [
	      'rgba(0, 255, 255, 0)',
	      'rgba(0, 255, 255, 1)',
	      'rgba(0, 191, 255, 1)',
	      'rgba(0, 127, 255, 1)',
	      'rgba(0, 63, 255, 1)',
	      'rgba(0, 0, 255, 1)',
	      'rgba(0, 0, 223, 1)',
	      'rgba(0, 0, 191, 1)',
	      'rgba(0, 0, 159, 1)',
	      'rgba(0, 0, 127, 1)',
	      'rgba(63, 0, 91, 1)',
	      'rgba(127, 0, 63, 1)',
	      'rgba(191, 0, 31, 1)',
	      'rgba(255, 0, 0, 1)'
	    ]
	    heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
    }
    
    //changes the radius
    var changeRadius = function() {
    	console.log("Heat Map Radius changed");
      	heatmap.set('radius', heatmap.get('radius') ? null : 20);
    }
    
    //changes the opacity
    var changeOpacity = function() {
    	console.log("Heat Map Opacity changed");
      	heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
    }
    
    //detects the enter key pressed in the search bar
    $("#search").keyup(function (e) {
        if (e.keyCode == 13) {
            search($("#search").val());
        }
    });
    
    //gets the data for the song query entered in the search bar
    var search = function(query){
        $.get("/songs?query=" + query, function(data){
      		loaded += data.length;
            console.log(data);
            $('#tbl_body').empty();
            pointArray.clear();
			data.forEach(function(el){
				pointArray.push(new google.maps.LatLng(el.latitude, el.longitude));
                $('#tbl_body').append("<tr><td>" + el.song + " - " + el.artiste + "</td><td>" + el.count + "</td></tr>");
			});
		});
    }

    //gets registration data from form and stores to database
    var getRegisterInfo = function() {
    	var email = $("#email_input").val();
    	var username = $("#username_input").val();
    	var password = $("#password_input").val();
        console.log(email + " " + password + " " + username);
        var regData = {};
        regData.email = email;
        regData.password = password;
        regData.username = username;
    	$.post("/user/register", regData, function(data){
            console.log(data);
            if(data === "Register successful"){
                document.cookie= "user=" + JSON.stringify(regData);
                window.location.href = "/";
            }
        }); 
	}
    
    //gets login information from form and stores to database
	var getLoginInfo = function() {
    	var email = $("#email_input").val();
    	var password = $("#password_input").val();
        var logData = {};
        logData.email = email;
        logData.password = password;
        $.post("/user/login", logData, function(data){
            console.log(data);
            if(data === "Login successful"){
                document.cookie= "user=" + JSON.stringify(logData);
                window.location.href = "/";
            }
        }); 
	}
    
    //changes password for the user
    var changePassword = function() {
        var user = JSON.parse(getCookie("user"));
        var email = user.email;
        var oldPass = $("#curr_pass").val();
        var newPass = $("#new_pass").val();
        var newData = {};
        newData.email = email;
        newData.oldpassword = oldPass;
        newData.newpassword = newPass;
        console.log(newData);
        $.ajax({
            url:"/user/password",
            type: "PUT",
            data: newData,
            success:function(data){
                console.log(data);
                if(data === "Password updated successfully"){
                    document.cookie= "user=" + JSON.stringify(logData);
                    window.location.href = "/";
                    $("#msg").text(data);
                }
            }
        });
    }
    
    //deletes the current user
    var deleteUser = function() {
        var user = JSON.parse(getCookie("user"));
        var email = user.email;
        var password = prompt("Enter your password","password");
        var newData = {};
        newData.email = email;
        newData.password = password;
        $.ajax({
            url:"/user",
            type: "DELETE",
            data: newData,
            success:function(data){
                console.log(data);
                if(data === "Account deleted"){
                    document.cookie= getCookie("_ga");;
                    window.location.href = "/login.html";
                }
            }
        });
    }

	
}(this));